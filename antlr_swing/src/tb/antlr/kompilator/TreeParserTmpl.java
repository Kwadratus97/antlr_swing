/**
 * 
 */
package tb.antlr.kompilator;

import org.antlr.runtime.RecognizerSharedState;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.TreeNodeStream;
import org.antlr.runtime.tree.TreeParser;

import tb.antlr.symbolTable.GlobalSymbols;

import tb.antlr.symbolTable.LocalSymbols;


/**
 * @author tb
 *
 */
public class TreeParserTmpl extends TreeParser {

	protected GlobalSymbols globals = new GlobalSymbols();
	protected LocalSymbols locals = new LocalSymbols();
	/**
	 * @param input
	 */
	public TreeParserTmpl(TreeNodeStream input) {
		super(input);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param input
	 * @param state
	 */
	public TreeParserTmpl(TreeNodeStream input, RecognizerSharedState state) {
		super(input, state);
		// TODO Auto-generated constructor stub
	}

	protected void errorID(RuntimeException ex, CommonTree id) {
		System.err.println(ex.getMessage() + " in line " + id.getLine());
	}
	
	protected Integer enterScope() {
		return locals.enterScope();
	}
	
	protected void leaveScope() {
		locals.leaveScope();
	}
	
	//zapis lokalnej lub globalnej tablicy symboli
	protected void deklaracjaZmiennej(Integer numberOfScope, String name) {
		
		//Jeśli jesteśmy w globalnym - czyli nie jestesmy w zadnym bloku
		if (numberOfScope == 1) 
		{
			//i nie ma w niej jeszcze zmiennej o takiej nazwie zmienna, ktorej szukamy
			if(globals.hasSymbol(name+1) == false)
			{
				System.out.println("nowy symbol globalny " + name+1);
				//tworzymy nową zmienną globalną
				globals.newSymbol(name+1);
			}
			else throw new RuntimeException("This variable exists in this scope!");
		}
		//Jeśli jesteśmy w jakimś bloku
		else if (numberOfScope >= 2) {
			//i nie ma w nim jeszcze zmiennej o takiej nazwie
			if(locals.hasSymbol(name+numberOfScope) == false) {
				//to ją możemy utworzyć
				locals.newSymbol(name + numberOfScope);
				System.out.println("nowy symbol lokalny " + name+numberOfScope);
			}
			else throw new RuntimeException("This variable exists in this scope!");
				
		}
	}
	
	//odczyt
	protected Integer odczytZmiennej(String name, Integer numberOfScope) {
		
		//Sprawdzenie, czy zmienna byla zadeklarowana		
		
		//może jest w tym samym bloku, wtedy wystarczy zwrocic jego nr
		if (locals.hasSymbol(name + numberOfScope)) 
			{
				System.out.println("zwracam: " + numberOfScope);
				return numberOfScope;
			}
		else //trzeba szukać jej w wyższych blokach
		{
			Integer pomocnicza = numberOfScope; //zmienna pomocnicza do przeszukiwania innych bloków, maleje gdy wspinamy się po blokach do góry
			while(pomocnicza>0)
			{
				System.out.println("pomocnicza: " + name+pomocnicza);
				if(locals.hasSymbolDepth(name+pomocnicza) != null) return pomocnicza;
				pomocnicza--;					
			}			
		}
		
		//Może jest aż w globalnych - wszystkie globalne są w bloku 1, więc mają nazwe z jedynką
		if(globals.hasSymbol(name+1))
		{
			return 1;
		}
			
		throw new RuntimeException("Cannot find this variable to read it!");
		
	}
	
	protected void zapisDoZmiennej(String name, Integer numberOfScope, String number)
	{
		
		Integer nrZmiennej = odczytZmiennej(name, numberOfScope);
		
		if(nrZmiennej == 1) 
			globals.setSymbol(name+numberOfScope, Integer.parseInt(number));
		else if (nrZmiennej >= 2)
			locals.setSymbol(name+numberOfScope, Integer.parseInt(number));

	}
	

}
