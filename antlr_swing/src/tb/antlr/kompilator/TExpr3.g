tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer numer = 0;
  
  Integer howManyScopes = 1; //globalny to 1
  Integer nrZmiennej = 0; //nijaki nr
  
  Integer licznikIf = 0;
}

prog: (e+=zakres | e+=expr | d+=decl | e+=ifinstr)* -> templateProgramu(name={$e},deklaracje={$d})
;


zakres: ^(BEGIN {howManyScopes=enterScope();} (e+=zakres | e+=expr | d+=decl | e+=ifinstr)* {leaveScope();}) 
        -> blok(wyr={$e}, dekl={$d})
;

//jak wyłączyć 3 argument - niezgodny?
ifinstr:
 ^(IF e1=expr e2=expr e3=expr) {licznikIf++;} -> rozumuj(kod={$e1.st},zgodny={$e2.st}, niezgodny={$e3.st}, nr={licznikIf.toString()})
  
 ;

decl:
        ^(VAR i1=ID) {deklaracjaZmiennej(howManyScopes, $ID.text);} -> dek(n={$ID.text + howManyScopes.toString()})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}

expr    : ^(PLUS  e1=expr e2=expr)                                -> dodaj(p1={$e1.st},p2={$e2.st})
        | ^(MINUS e1=expr e2=expr)                                -> odejmij(p1={$e1.st},p2={$e2.st})
        | ^(MUL   e1=expr e2=expr)                                -> mnoz(p1={$e1.st},p2={$e2.st})
        | ^(DIV   e1=expr e2=expr)                                -> dziel(p1={$e1.st},p2={$e2.st})
        | ^(PODST i1=ID   e2=expr)    {zapisDoZmiennej($ID.text, howManyScopes, $e2.text);}   -> podst( i={$i1 + howManyScopes.toString()},p={$e2.st}) 
        | INT  {numer++;}                                         -> int(i={$INT.text},j={numer.toString()})
        | ID {nrZmiennej=odczytZmiennej($ID.text, howManyScopes);} -> id(n={$ID.text + nrZmiennej.toString()})
    ;
    
    
    
    
    