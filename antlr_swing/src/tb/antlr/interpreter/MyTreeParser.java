package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;
import tb.antlr.symbolTable.*;

public class MyTreeParser extends TreeParser {

	GlobalSymbols globalSymbols = new GlobalSymbols();
	
    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	
	protected Integer add(Integer a, Integer b) {
		return a+b;
	}
	
	protected Integer sub(Integer a, Integer b) {
		return a-b;
	}
	
	protected Integer mul(Integer a, Integer b) {
		return a*b;
	}
	
	protected Integer div(Integer a, Integer b) {
		return a/b;
	}
	
	protected Integer podstaw(String n, Integer v) {
		globalSymbols.setSymbol(n,v);
		return v;
	}
	
	protected void declareVar(String n) {
		globalSymbols.newSymbol(n);
	}
}
